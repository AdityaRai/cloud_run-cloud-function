resource "google_cloud_run_service" "default" {
  name     = "hello"
  location = "us-central1"
  project  = "splendid-parsec-356405"

  template {
    spec {
      containers {
        image = "gcr.io/cloudrun/hello"
      }
    }
  }
}

resource "google_cloud_run_service_iam_member" "member" {
  location = google_cloud_run_service.default.location
  project  = google_cloud_run_service.default.project
  service  = google_cloud_run_service.default.name
  role     = "roles/run.invoker"
  member   = "allUsers"
}


resource "google_storage_bucket" "bucket" {
  name     = "a-test-bucket045"
  location = "US"
}

resource "google_storage_bucket_object" "archive" {
  name   = "index.zip"
  bucket = google_storage_bucket.bucket.name
  source = "C:/Users/HP/Desktop/Cloud_run & cloud_fun/index.zip"
}

resource "google_cloudfunctions_function" "function" {
  name        = "a-test-bucket"
  description = "My function"
  runtime     = "nodejs16"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.bucket.name
  source_archive_object = google_storage_bucket_object.archive.name
  trigger_http          = true
  entry_point           = "helloWorld"
}


output "cloud_run_url" {
  value = element(google_cloud_run_service.default.status, 0).url
}